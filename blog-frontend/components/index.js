Vue.component('index-page', {
  props: ['userToken', 'isSuperuser'],
  template: `<div class="row">
              <div class="col-sm-4" style="max-height:500px; overflow-y: auto;">
                <div class="list-group">
                  <template v-for="(item, index) in items">
                    <a href="#" v-on:click="displayBlog(index)" class="list-group-item"
                       v-bind:class="{active: index==activeIndex}">
                      <h4 class="list-group-item-heading">{{item.title}}</h4>
                      <p class="list-group-item-text">{{item.content}}</p>
                      <p class="list-group-item-text text-right">{{item.author_name==' '?'N/A':item.author_name}}</p>
                    </a>
                  </template>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <h2 style="text-transform: capitalize;">{{displayBlogTitle}}</h2>
                    <p class="text-right"><i>By - {{displayBlogAuthor==' '?'N/A':displayBlogAuthor}}</i></p>
                    <p>{{displayBlogContent}}</p>
                    <hr/>
                    <div class="form-group" v-if="showCommentBox">
                      <label>Your comment</label>
                      <div><textarea class="form-control" v-model="comment"></textarea></div><br/>
                      <div><button class="btn btn-primary" @click="submitComment">submit</button></div>
                    </div>
                    <template>
                     <span><a href="#" @click="showCommentBox=true">Comment</a> | </span>
                    </template>
                    <template v-if="isAuthor|isSuperuser">
                     <span v-if="isArchived"><a href="#" @click="archivePost(false)">Unarchive</a> | </span>
                     <span v-else><a href="#" @click="archivePost()">Archive</a> | </span>  
                    </template>
                    <template v-if="isSuperuser">
                     <span v-if="isPublished"><a href="#" @click="publishPost(false)">Unpublish</a></span>
                     <span v-else><a href="#" @click="publishPost()">Publish</a></span>
                    </template>
                    <template v-if="activeComments.length>0">
                      <hr/>
                      <h4>Comments</h4>
                      <div v-for="cmnt in activeComments" style="margin-left: 10px; padding: 8px; border-bottom: 1px solid #eee;">
                        <div>{{cmnt.comment}}</div>
                        <div style="font-size: 12px;"><strong>{{(cmnt.user.first_name+' '+cmnt.user.last_name)==' '?'N/A':(cmnt.user.first_name+' '+cmnt.user.last_name)}}</strong></div>
                        <div v-if="cmnt.is_commentor||isSuperuser"><a href="#" @click="deleteComment(cmnt.id)">Delete</a></div>
                      </div>
                    </template>
                   
                  </div>
                </div>
              </div>
            </div>`,
  data() {
    return {
      items: [],
      activeId: null,
      activeIndex: 0,
      displayBlogTitle: '',
      displayBlogContent: '',
      displayBlogAuthor: '',
      isAuthor: false,
      isArchived: false,
      isPublished: false,
      showCommentBox: false,
      activeComments: [],
      comment: ''
    }
  },
  created() {
    this.getData(0)
  },
  methods: {
    getData(activeIndex) {
      axios.defaults.headers.common['Authorization'] = 'Token ' + this.userToken
      axios.get('http://localhost:8000/api/blog/')
        .then((response) => {
          this.items = response.data
          this.activeIndex = activeIndex
          this.activeId = this.items[activeIndex].id
          this.displayBlogTitle = this.items[activeIndex].title
          this.displayBlogContent = this.items[activeIndex].content
          this.displayBlogAuthor = this.items[activeIndex].author_name
          this.isArchived = this.items[activeIndex].archive
          this.isAuthor = this.items[activeIndex].is_author
          this.isPublished = this.items[activeIndex].publish
          this.activeComments = this.items[activeIndex].comments
        })
        .catch((error) => {
          console.log(error)
        });
    },
    displayBlog(index) {
      this.activeId = this.items[index].id
      this.activeIndex = index
      this.displayBlogTitle = this.items[index].title
      this.displayBlogContent = this.items[index].content
      this.displayBlogAuthor = this.items[index].author_name
      this.isArchived = this.items[index].archive
      this.isAuthor = this.items[index].is_author
      this.isPublished = this.items[index].publish
      this.activeComments = this.items[index].comments
      this.showCommentBox = false
    },
    archivePost(status = true) {
      axios.defaults.headers.common['Authorization'] = 'Token ' + this.userToken
      axios.patch('http://localhost:8000/api/blog/' + this.activeId + '/', {archive: status})
        .then((response) => {
          this.$parent.generateNotification('Successfully changed archive status.')
          this.getData(this.activeIndex)
        })
        .catch((error) => {
          this.$parent.generateNotification('Failed changing archive status.', 'alert-danger')
        });
    },
    publishPost(status = true) {
      axios.defaults.headers.common['Authorization'] = 'Token ' + this.userToken
      axios.patch('http://localhost:8000/api/blog/' + this.activeId + '/publish/', {publish: status})
        .then((response) => {
          this.$parent.generateNotification('Successfully changed publish status.')
          this.getData(this.activeIndex)
        })
        .catch((error) => {
          this.$parent.generateNotification('Failed changing publish status.', 'alert-danger')
        });
    },
    submitComment() {
      axios.defaults.headers.common['Authorization'] = 'Token ' + this.userToken
      let payload = {
        blog: this.activeId,
        comment: this.comment
      }
      axios.post('http://localhost:8000/api/comment/', payload)
        .then((response) => {
          this.$parent.generateNotification('Successfully added new comment.')
          this.comment = ''
          this.showCommentBox = false
          this.getData(this.activeIndex)
        })
        .catch((error) => {
          this.$parent.generateNotification('Failed adding new comment.', 'alert-danger')
        });
    },
    deleteComment(id){
      axios.delete('http://localhost:8000/api/comment/'+id+'/')
        .then((response) => {
          this.$parent.generateNotification('Successfully deleted comment.')
          this.getData(this.activeIndex)
        })
        .catch((error) => {
          this.$parent.generateNotification('Failed deleting comment.', 'alert-danger')
        });
    }
  }
})