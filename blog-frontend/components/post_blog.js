Vue.component('post-blog-page', {
  props: ['userToken'],
  data() {
    return {
      postError: null,
      form:{
        title: '',
        content: '',
        archive: '0'
      }
    }
  },
  created() {
    this.$parent.generateNotification('')
  },
  methods: {
    submitForm() {
      axios.defaults.headers.common['Authorization'] = 'Token '+this.userToken
      axios.post('http://localhost:8000/api/blog/', this.form)
        .then((response) => {
          this.$parent.generateNotification('Successfully posted new blog.')
          this.$parent.changePage('index')
        })
        .catch((error) => {
          this.postError = 'Failed to post the form.'
        });
    }
  },
  template: `<div class="row">
              <div class="col-sm-12">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Add new blog
                </div>
                <div class="panel-body">
                  <div id="login-alert" class="alert alert-danger col-sm-12" v-if="postError">{{postError}}</div>
                  <form>
                    <div class="form-group">
                      <label for="blog-title">Title</label>
                      <input id="blog-title" v-model="form['title']" name="title" type="text" class="form-control" placeholder="Blog Title">
                    </div>
                    <div class="form-group">
                      <label for="blog-content">Content</label>
                      <textarea id="blog-content" v-model="form['content']" name="content" class="form-control" placeholder="Blog Content"></textarea>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="1" v-model="form['archive']">Archive</label>
                    </div>
                    <div class="form-group">
                      <button type="button" class="btn btn-danger">Cancel</button>
                      <button type="button" class="btn btn-primary" @click="submitForm">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
              </div>
            </div>`
})