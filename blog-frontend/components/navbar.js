Vue.component('navbar', {
  template: `<nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">BlogShare</a>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                      <li v-bind:class="{active: currentPage=='index'}"><a href="#" @click="$parent.changePage('index')">Home <span class="sr-only">(current)</span></a></li> 
                      <li v-bind:class="{active: currentPage=='post-blog'}"><a href="#" @click="$parent.changePage('post-blog')">Post Blog</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                      <li><a href="#">View Profile</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Actions <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Update Profile</a></li>
                          <li><a href="#">Change Password</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="#" @click="$parent.logoutUser()">Logout</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
              `,
  props: ['currentPage'],
  methods: {
  }
})