from django.db import models
from django.contrib.auth.models import User


class Blog(models.Model):
    author = models.ForeignKey(User, related_name='blogs',
                               on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    content = models.TextField()
    archive = models.BooleanField(default=False)
    publish = models.BooleanField(default=False)

    def author_name(self):
        return self.author.first_name + ' ' + self.author.last_name

    def __str__(self):
        return self.title


class Comment(models.Model):
    blog = models.ForeignKey(Blog, related_name='comments',
                             on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='comments',
                             on_delete=models.CASCADE)
    comment = models.TextField()

    def __str__(self):
        return self.comment

    class Meta:
        ordering = ('-id',)
