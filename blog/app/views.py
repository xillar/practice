from rest_framework import generics
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.db.models import Q
from .serializers import (
    BlogSerializer,
    CommentSerializer,
    PublishBlogSerializer
)
from app.models import (
    Blog,
    Comment
)
from rest_framework.permissions import IsAuthenticated, BasePermission, \
    IsAdminUser
from rest_framework.authentication import TokenAuthentication
from rest_framework.pagination import LimitOffsetPagination


class IsCommentorOrSuperuser(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user or request.user.is_superuser


class IsAuthorOrSuperuser(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user == obj.author or request.user.is_superuser


class BlogList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = BlogSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = Blog.objects.all()
        else:
            queryset = Blog.objects.filter(
                Q(author=self.request.user) | (
                        Q(publish=True) & Q(archive=False)))
        return queryset.order_by('-id')


class BlogDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer

    def get_permissions(self):
        if self.request.method == 'GET':
            return [IsAuthenticated(), ]
        else:
            return [IsAuthenticated(), IsAuthorOrSuperuser()]


class BlogPublish(generics.UpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAdminUser, ]
    queryset = Blog.objects.all()
    serializer_class = PublishBlogSerializer


class CommentList(generics.CreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class CommentDetail(generics.DestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated, IsCommentorOrSuperuser]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'is_superuser': user.is_superuser,
            'first_name': user.first_name,
            'last_name': user.last_name
        })
