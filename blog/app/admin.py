from django.contrib import admin
from app.models import (
    Blog,
    Comment
)


class BlogAdmin(admin.ModelAdmin):
    list_display = ('title', 'archive', 'publish')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('blog', 'comment')


admin.site.register(Blog, BlogAdmin)
admin.site.register(Comment, CommentAdmin)
