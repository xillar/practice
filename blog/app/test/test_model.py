from django.test import TestCase
from django.contrib.auth.models import User
from app.models import (
    Blog,
    Comment
)


class BlogTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create(username='amit', password='passmeword')
        user2 = User.objects.create(username='amit2', password='passmeword2')
        Blog.objects.create(author=user1, title='Test blog',
                            content='Test blog content.')
        Blog.objects.create(author=user2, title='Test blog 2',
                            content='Test blog content 2.')

    def test_blog_is_not_published(self):
        blog1 = Blog.objects.get(title__iexact='Test blog')
        blog2 = Blog.objects.get(title__iexact='test blog 2')

        self.assertEqual(blog1.publish, False)
        self.assertEqual(blog2.publish, False)

    def test_blog_is_not_archived(self):
        blog1 = Blog.objects.get(title__iexact='Test blog')
        blog2 = Blog.objects.get(title__iexact='test blog 2')

        self.assertEqual(blog1.archive, False)
        self.assertEqual(blog2.archive, False)


class CommentTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create(username='amit', password='passmeword')
        user2 = User.objects.create(username='amit2', password='passmeword2')
        blog1 = Blog.objects.create(author=user1, title='Test blog',
                                    content='Test blog content.')
        blog2 = Blog.objects.create(author=user2, title='Test blog 2',
                                    content='Test blog content 2.')
        Comment.objects.create(user=user1, blog=blog1,
                               comment="Test comment 1.")
        Comment.objects.create(user=user2, blog=blog2,
                               comment="Test comment 2.")

    def test_commentor(self):
        comment1 = Comment.objects.get(comment__iexact='test commeNt 1.')
        comment2 = Comment.objects.get(comment__iexact='test commENT 2.')

        self.assertEqual(comment1.user.username, 'amit')
        self.assertEqual(comment2.user.username, 'amit2')

    def test_comment_blog_author(self):
        comment1 = Comment.objects.get(comment__iexact='test commeNt 1.')
        comment2 = Comment.objects.get(comment__iexact='test commENT 2.')

        self.assertEqual(comment1.blog.author.username, 'amit')
        self.assertEqual(comment2.blog.author.username, 'amit2')
