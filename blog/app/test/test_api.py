from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from app.models import (
    Blog,
    Comment
)


class BlogAPIViewTestCase(APITestCase):

    def createUser(self):
        user = {
            'username': 'amit',
            'password': 'password'
        }
        self.user = User.objects.create(**user)
        self.token = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token[0].key)

    def setUp(self):
        self.createUser()

    def test_blog_create(self):
        blog = {
            'title': 'Test blog.',
            'content': 'Test blog content.'
        }
        response = self.client.post('/api/blog/', blog, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_blog_list(self):
        blogs = [
            {
                'title': 'Test blog.',
                'content': 'Test blog content.',
                'author': self.user
            },
            {
                'title': 'Test blog 2.',
                'content': 'Test blog content 2.',
                'author': self.user
            },
            {
                'title': 'Test blog 3.',
                'content': 'Test blog content 3.',
                'author': self.user
            }
        ]
        for blog in blogs:
            Blog.objects.create(**blog)
        response = self.client.get('/api/blog/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_blog_retrieve(self):
        data = {
            'title': 'Test blog.',
            'content': 'Test blog content.',
            'author': self.user
        }

        blog = Blog.objects.create(**data)
        response = self.client.get('/api/blog/' + str(blog.id) + '/',
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Test blog.')
        self.assertEqual(response.data['content'], 'Test blog content.')

    def test_blog_update(self):
        data = {
            'title': 'Test blog.',
            'content': 'Test blog content.',
            'author': self.user,
            'archive': True
        }

        blog = Blog.objects.create(**data)
        response = self.client.patch('/api/blog/' + str(blog.id) + '/',
                                     {'archive': False}
                                     )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        blog = Blog.objects.get(pk=blog.id)
        self.assertEqual(blog.archive, False)

    def test_blog_destroy(self):
        data = {
            'title': 'Test blog.',
            'content': 'Test blog content.',
            'author': self.user,
            'archive': True
        }

        blog = Blog.objects.create(**data)
        response = self.client.delete('/api/blog/' + str(blog.id) + '/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        with self.assertRaises(Blog.DoesNotExist):
            Blog.objects.get(pk=blog.id)


class CommentTestCase(APITestCase):

    def createUser(self):
        user = {
            'username': 'amit',
            'password': 'password'
        }
        self.user = User.objects.create(**user)
        self.token = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token[0].key)

    def createBlog(self):
        blogs = [
            {
                'title': 'Test blog.',
                'content': 'Test blog content.',
                'author': self.user
            },
            {
                'title': 'Test blog 2.',
                'content': 'Test blog content 2.',
                'author': self.user
            },
            {
                'title': 'Test blog 3.',
                'content': 'Test blog content 3.',
                'author': self.user
            }
        ]
        for blog in blogs:
            Blog.objects.create(**blog)

    def setUp(self):
        self.createUser()
        self.createBlog()

    def test_comment_create(self):
        comment = {
            'blog': Blog.objects.get(title__iexact='test blog.').id,
            'user': self.user.id,
            'comment': 'This is comment.'
        }
        wrong_comment = {
            'blog': 1000,
            'user': self.user.id,
            'comment': 'This is comment.'
        }

        response = self.client.post('/api/comment/', comment, format='json')
        self.assertEqual(response.status_code, 201)

        response = self.client.post('/api/comment/', wrong_comment,
                                    format='json')
        self.assertEqual(response.status_code, 400)

    def test_comment_destroy(self):
        comment = {
            'blog': Blog.objects.get(title__iexact='test blog.'),
            'user': self.user,
            'comment': 'This is comment.'
        }

        comment = Comment.objects.create(**comment)
        response = self.client.delete(f'/api/comment/{comment.id}/',
                                      format='json')
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Comment.DoesNotExist):
            Comment.objects.get(pk=comment.id)
