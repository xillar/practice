from rest_framework import serializers
from django.contrib.auth.models import User
from app.models import (
    Blog,
    Comment
)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name'
        )


class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    is_commentor = serializers.SerializerMethodField(read_only=True)

    def get_is_commentor(self, obj):
        return obj.user == self.context['request'].user

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)

    class Meta:
        model = Comment
        fields = (
            'id',
            'user',
            'blog',
            'comment',
            'is_commentor'
        )
        read_only_fields = ('id',)
        extra_kwargs = {
            'blog': {'write_only': True}
        }


class BlogSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(read_only=True, many=True)
    is_author = serializers.SerializerMethodField(read_only=True)

    def get_is_author(self, obj):
        return obj.author == self.context['request'].user

    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        return super().create(validated_data)

    class Meta:
        model = Blog
        fields = (
            'id',
            'title',
            'content',
            'author',
            'author_name',
            'archive',
            'comments',
            'publish',
            'is_author'
        )
        read_only_fields = ('id', 'author', 'author_name')


class PublishBlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = ('publish',)
