import logging
from django.db.models.signals import pre_save
from django.dispatch import receiver
from app.models import (
    Blog,
    Comment
)

logging.basicConfig(filename='logs.txt', level=logging.DEBUG,
                    format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


@receiver(pre_save, sender=Blog)
@receiver(pre_save, sender=Comment)
def my_handler(sender, **kwargs):
    if sender == Blog:
        logging.info(
            f"Created blog object of title: {kwargs['instance'].title}")
    elif sender == Comment:
        logging.info(
            f"Created comment object: {kwargs['instance'].comment} by user: {kwargs['instance'].user}")
