from django.urls import path
from .views import (
    BlogList,
    BlogDetail,
    BlogPublish,
    CommentList,
    CommentDetail
)

urlpatterns = [
    path('blog/<int:pk>/publish/', BlogPublish.as_view()),
    path('blog/<int:pk>/', BlogDetail.as_view()),
    path('blog/', BlogList.as_view()),
    path('comment/<int:pk>/', CommentDetail.as_view()),
    path('comment/', CommentList.as_view()),
]
