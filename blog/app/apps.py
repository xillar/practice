from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'app'

    def ready(self):
        from django.db.models.signals import pre_save
        from .signals import receiver
        pre_save.connect(receiver, sender='app.Blog')
        pre_save.connect(receiver, sender='app.Comment')
